import { Component, OnInit } from '@angular/core';

interface activity {
  time: string;
  ringColor: string;
  message: string;
}

export interface PeriodicElement {
  id: number;
  name: string;
  work: string;
  project: string;
  priority: string;
  badge: string;
  budget: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { id: 1, name: 'Deep Javiya', work: 'Frontend Devloper', project: 'Flexy Angular', priority: 'Low', badge: 'badge-info', budget: '$3.9k' },
  { id: 2, name: 'Nirav Joshi', work: 'Project Manager', project: 'Hosting Press HTML', priority: 'Medium', badge: 'badge-primary', budget: '$24.5k' },
  { id: 3, name: 'Sunil Joshi', work: 'Web Designer', project: 'Elite Admin', priority: 'High', badge: 'badge-danger', budget: '$12.8k' },
  { id: 4, name: 'Maruti Makwana', work: 'Backend Devloper', project: 'Material Pro', priority: 'Critical', badge: 'badge-success', budget: '$2.4k' },
];

@Component({
  selector: 'app-daily',
  templateUrl: './daily.component.html',
  styleUrls: ['./daily.component.css'],
})
export class DailyComponent implements OnInit {

  displayedColumns: string[] = ['id', 'assigned', 'name', 'priority', 'budget'];
  dataSource = ELEMENT_DATA;

  constructor() {}

  ngOnInit(): void {}

  activity: activity[] = [
    {
      time: '09.50',
      ringColor: 'ring-success',
      message: 'Meeting with John',
    },
    {
      time: '09.46',
      ringColor: 'ring-primary',
      message: 'Payment received from John Doe of $385.90',
    },
    {
      time: '09.47',
      ringColor: 'ring-info',
      message: 'Project Meeting',
    },
    {
      time: '09.48',
      ringColor: 'ring-warning',
      message: 'New Sale recorded #ML-3467',
    },
    {
      time: '09.49',
      ringColor: 'ring-danger',
      message: 'Payment was made of $64.95 to Michael Anderson',
    },
  ];
}
