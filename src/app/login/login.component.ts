import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  public loginForm!: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private api: HttpClient,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  login() {
    this.api.get<any>('http://localhost:3000/login/').subscribe(
      (res) => {
        const user = res.find((a: any) => {
          return (
            a.username === this.loginForm.value.username &&
            a.password === this.loginForm.value.password
          );
        });
        if (user) {
          alert('Login successfuly');
          this.loginForm.reset();
          this.router.navigate(['/sidenav','dashboard']);
        } else {
          alert('User not found');
        }
      },
      (err) => {
        alert('something wrong');
      }
    );
  }
}
