import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../service/api.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { format } from 'date-fns';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css'],
})
export class DialogComponent implements OnInit {
  status: string[] = ['Active', 'Inactive'];
  employeeForm!: FormGroup;
  actionBtn: string = 'Save';
  selectedDate!: Date;
  maxDate!: Date;

  constructor(
    private formBuilder: FormBuilder,
    private api: ApiService,
    @Inject(MAT_DIALOG_DATA) public editData: any,
    private dialogRef: MatDialogRef<DialogComponent>
  ) {}

  onDateChange() {
    const formattedDate = format(this.selectedDate, 'dd-MM-yyyy');
    console.log(formattedDate);
  }

  ngOnInit(): void {
    this.maxDate = new Date();
    this.employeeForm = this.formBuilder.group({
      username: ['', Validators.required],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: new FormControl('', [Validators.required, Validators.email]),
      birthDate: ['', Validators.required],
      basicSalary: ['', Validators.required],
      status: ['', Validators.required],
      group: ['', Validators.required],
      description: [''],
    });

    if (this.editData) {
      this.actionBtn = 'Update';
      this.employeeForm.controls['username'].setValue(this.editData.username);
      this.employeeForm.controls['firstname'].setValue(this.editData.firstname);
      this.employeeForm.controls['lastname'].setValue(this.editData.lastname);
      this.employeeForm.controls['email'].setValue(this.editData.email);
      this.employeeForm.controls['birthDate'].setValue(this.editData.birthDate);
      this.employeeForm.controls['basicSalary'].setValue(
        this.editData.basicSalary
      );
      this.employeeForm.controls['status'].setValue(this.editData.status);
      this.employeeForm.controls['group'].setValue(this.editData.group);
      this.employeeForm.controls['description'].setValue(
        this.editData.description
      );
    }
  }

  getEmailErrorMessage() {
    const emailControl = this.employeeForm.get('email');

    if (emailControl?.hasError('required')) {
      return 'Email is required';
    }

    return emailControl?.hasError('email') ? 'Invalid email' : '';
  }

  addEmployee() {
    if (!this.editData) {
      if (this.employeeForm.valid) {
        this.api.postEmployee(this.employeeForm.value).subscribe({
          next: (res) => {
            alert('Employee added successfuly');
            this.employeeForm.reset();
            this.dialogRef.close('save');
          },
          error: () => {
            alert('error adding Employee');
          },
        });
      }
    } else {
      this.updateEmployee();
    }
  }

  updateEmployee() {
    this.api
      .putEmployee(this.employeeForm.value, this.editData.id)
      .subscribe({
        next: (res) => {
          alert('Employee update successfuly');
          this.employeeForm.reset();
          this.dialogRef.close('update');
        },
        error: () => {
          alert('Error ehile updating the record! ');
        },
      });
  }
}
